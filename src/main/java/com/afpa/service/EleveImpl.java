package com.afpa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.afpa.dao.IDaoCours;
import com.afpa.dao.IDaoEleve;
import com.afpa.dao.IDaoRole;
import com.afpa.dao.IDaoTuteur;
import com.afpa.dao.IDaoUser;
import com.afpa.dto.CoursDto;
import com.afpa.dto.EleveDto;
import com.afpa.entity.cours.Cours;
import com.afpa.entity.personne.Eleve;
import com.afpa.entity.personne.Tuteur;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.security.model.User;
 @Service
public class EleveImpl implements IEleveService {

	 @Autowired
	 IDaoEleve eleveDao;
	 @Autowired
	 IDaoTuteur tuteurDao;
	 @Autowired
	 IDaoCours coursDao;
	 @Autowired
	 IDaoRole roleDao;
	 @Autowired
	 IDaoUser userDao;
	 @Autowired
	 BCryptPasswordEncoder bcrypt;
	 
	@Autowired
	ITuteurService tuteurServ;
	 
	 @Autowired
	 ModelMapper model;
	 
	@Override
	public List<EleveDto> listeEleves()  throws NullException{
		
		List<Eleve> listElvs = (List<Eleve>) this.eleveDao.findAll();
		if(listElvs == null) {
			throw new NullException();
		}else {
			List<EleveDto> mesEleve = (listElvs)
					.stream()
					.map(e -> { 
						EleveDto edto = new EleveDto();
						edto.setId(e.getId());
						edto.setNom(e.getNom());
						edto.setPrenom(e.getPrenom());
						return edto;
					})
					.collect(Collectors.toList());
			return mesEleve;
		}
	}

	@Override
	public EleveDto eleveById(int i)  throws NullException{
		Eleve el = eleveDao.findById(i).get();
		if(el == null) {
			throw new NullException();
		}else {
			
		return model.map(el, EleveDto.class);
		}
	}

	@Override
	public EleveDto addEleve(EleveDto eleve) throws AjoutVideException, NullException {
		Eleve monEleve = model.map(eleve, Eleve.class);
		if(monEleve == null || monEleve.getNom() == null || monEleve.getPrenom() == null || monEleve.getTuteur() == null || monEleve.getTuteur().getId() == 0) {
			throw new AjoutVideException();
		}else {
			
		}
		List<Tuteur> listTuteur = tuteurDao.findByNomAndPrenom(eleve.getNom(),eleve.getPrenom());
		List<Eleve> listEleve = eleveDao.findByNomAndPrenom(eleve.getNom(),eleve.getPrenom());
		int nbr = listTuteur.size() + listEleve.size();
		String username = eleve.getPrenom() + "." + eleve.getNom();
		if(nbr != 0) {
			username = username + "20" + nbr;
		}
		monEleve.setUser(userDao.save(User.builder().username(eleve.getPrenom() + "." + eleve.getNom()).password(bcrypt.encode("1234")).role(roleDao.findByLabel("ELEVE").get()).build()));
		EleveDto e = model.map(eleveDao.save(monEleve),EleveDto.class);
		this.tuteurServ.AddElevesListeTuteur(e.getId(), eleve.getTuteur().getId());
		return e;
		
		
	}

	@Override
	public EleveDto putEleve(EleveDto eleve)  throws NullException{
		
		Eleve el = this.eleveDao.findById(eleve.getId()).get();
		Tuteur tu = this.tuteurDao.findById(eleve.getTuteur().getId()).get();
		if(el == null || tu == null) {
			throw new NullException();
		}else {
			
			el.setNom(eleve.getNom());
			el.setPrenom(eleve.getPrenom());
			this.changetutu(el.getId(),el.getTuteur().getId(), tu.getId(), el);
			el.setTuteur(tu);
			eleveDao.save(el);
			return null;
		}
	}

	@Override
	public String deleteEleve(int id)  throws NullException{
		Eleve el = this.eleveDao.findById(id).get();
		if(el == null) {
			throw new NullException();
		}else {
			
		List<Eleve> liste = el.getTuteur().getEleves();
		liste.remove(el);
		eleveDao.supprdeLaListeDesCours(id);
		eleveDao.deleteById(id);
		return "ok bien supprimé";
		}
	}
	
	
	public void changetutu(int idE,int idT, int idTN, Eleve e) {
		List<Eleve> eL = this.tuteurDao.findById(idT).get().getEleves();
		boolean test = false;
		Eleve monEleve = null;
		for (Eleve eleve : eL) {
			if(eleve.getId() == idE) {
				monEleve = eleve;
				test =	true;
			}
		}
		if(test) {
			eL.remove(monEleve);
			if(test) {
				List<Eleve> newEleve = this.tuteurDao.findById(idTN).get().getEleves();
				newEleve.add(e);
			}
		}
	
	}
	
	//ajoute un  cours dans la liste de cours de l'élève
	@Override
	public void addcours(int idE, int idC) {
		Eleve el = this.eleveDao.findById(idE).get();
		Cours co = this.coursDao.findById(idC).get();
		List<Cours> cours = el.getListCours();
		cours.add(co);
		this.eleveDao.save(el);
	}
	
	//supprime un  cours dans la liste de cours de l'élève
	@Override
		public void deletecours(int idE, int idC) {
			Eleve el = this.eleveDao.findById(idE).get();
			Cours co = this.coursDao.findById(idC).get();
			List<Cours> cours = el.getListCours();
			cours.remove(co);
			this.eleveDao.save(el);
		}

	@Override
	public Integer nbrEleve() {
		return (int) this.eleveDao.count();
	}

	@Override
	public List<CoursDto> coursListe(int id)  throws NullException{
		List<Cours> c = eleveDao.listeCoursById(id);
		if(c == null) {
			throw new NullException();
		}else {
			
			System.err.println(c);
			return null;
		}
		
	}
	

}
