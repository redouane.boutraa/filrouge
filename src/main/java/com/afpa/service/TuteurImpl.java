package com.afpa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.afpa.dao.IDaoEleve;
import com.afpa.dao.IDaoRole;
import com.afpa.dao.IDaoTuteur;
import com.afpa.dao.IDaoUser;
import com.afpa.dto.AdresseDto;
import com.afpa.dto.EleveDto;
import com.afpa.dto.TuteurDto;
import com.afpa.entity.personne.Adresse;
import com.afpa.entity.personne.Eleve;
import com.afpa.entity.personne.Tuteur;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.security.model.User;
@Service
public class TuteurImpl implements ITuteurService {
	
	@Autowired
	IDaoTuteur tdao;
	
	@Autowired
	IDaoEleve edao;
	
	@Autowired
	IDaoUser userDao;
	
	@Autowired
	IDaoRole roleDao;
	
	@Autowired
	BCryptPasswordEncoder bcrypt;
	
	 @Autowired
	 ModelMapper model;
	

	@Override
	public List<TuteurDto> listeTuteurs() throws NullException{
		List<Tuteur> listTut = (List<Tuteur>) tdao.findAll();
		if(listTut == null){
			throw new NullException();
		} else {
			
			List<TuteurDto> mesTuteurs = (listTut)
					.stream()
					.map(e -> {
						TuteurDto t = new TuteurDto();
						t.setId(e.getId());
						t.setPrenom(e.getPrenom());
						t.setNom(e.getNom());
						return t;
					})
					.collect(Collectors.toList());
			return mesTuteurs;
		}
	}


	@Override
	public TuteurDto tuteurById(int i) throws NullException{
		Tuteur e = tdao.findById(i).get();
		if(e == null){
			throw new NullException();
		} else {
			
		TuteurDto t = new TuteurDto();
		t.setId(e.getId());
		t.setPrenom(e.getPrenom());
		t.setNom(e.getNom());
		t.setDateNaissance(e.getDateNaissance());
		t.setAdress(AdresseDto.builder()
				.ville(e.getAdress().getVille())
				.codePostal(e.getAdress().getCodePostal())
				.pays(e.getAdress().getPays())
				.build());
		t.setEmail(e.getEmail());
		t.setNumTel(e.getNumTel());
		t.setDateNaissance(e.getDateNaissance());
		
		List<EleveDto> elevesDto = e.getEleves().stream()
		.map(ee-> {
			EleveDto ed = new EleveDto();
			ed.setId(ee.getId());
			ed.setPrenom(ee.getPrenom());
			ed.setNom(ee.getNom());
			return ed;
		})
		.collect(Collectors.toList());
		t.setEleve(elevesDto);
		return t;
		}
	}


	@Override
	public TuteurDto addTuteur(TuteurDto tuteur) throws AjoutVideException{
		if(tuteur == null || tuteur.getNom() == null || tuteur.getPrenom() == null|| tuteur.getNumTel() == null || tuteur.getEmail() == null) {
			throw new AjoutVideException();
		}else {
			Tuteur monTuteur = model.map(tuteur, Tuteur.class);
			List<Tuteur> listTuteur = tdao.findByNomAndPrenom(tuteur.getNom(),tuteur.getPrenom());
			List<Eleve> listEleve = edao.findByNomAndPrenom(tuteur.getNom(),tuteur.getPrenom());
			int nbr = listTuteur.size() + listEleve.size();
			String username = tuteur.getPrenom() + "." + tuteur.getNom();
			if(nbr != 0) {
				username = username + "20" + nbr;
			}
			monTuteur.setUser(userDao.save(User.builder().username(username).password(bcrypt.encode("1234")).role(roleDao.findByLabel("TUTEUR").get()).build()));
			return model.map(tdao.save(monTuteur),TuteurDto.class);
		}
	}

	
	@Override
	public String putTuteur(TuteurDto tuteur) throws NullException{
		Tuteur tu = this.tdao.findById(tuteur.getId()).get();
		if(tu == null){
			throw new NullException();
		} else {
		
		tu.setNom(tuteur.getNom());
		tu.setPrenom(tuteur.getPrenom());
		tu.setNumTel(tuteur.getNumTel());
		tu.setEmail(tuteur.getEmail());
		tu.setAdress(Adresse.builder()
				.rue(tuteur.getAdress().getRue())
				.ville(tuteur.getAdress().getVille())
				.codePostal(tuteur.getAdress().getCodePostal())
				.pays(tuteur.getAdress().getPays())
				.build());
		tdao.save(tu);
		return "ok";
		}
	}
	
	
	@Override
	public String AddElevesListeTuteur(int idE, int idT) throws NullException{
		Eleve e = this.edao.findById(idE).get();
		Tuteur tu = this.tdao.findById(idT).get();
		if(tu == null || e == null){
			throw new NullException();
		} else {
		tu.getEleves().add(e);
		tdao.save(tu);
		return "ok";
		}
	}
	
	@Override
	public String deleteTuteur(int parseInt) throws NullException{
		tdao.deleteById(parseInt);
		return "ok";
	}


	@Override
	public Integer nbrTuteur() {
		return (int)this.tdao.count();
		}
}
