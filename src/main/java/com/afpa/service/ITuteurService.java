package com.afpa.service;

import java.util.List;

import com.afpa.dto.TuteurDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

public interface ITuteurService {

	public String deleteTuteur(int parseInt) throws NullException;

	public List<TuteurDto> listeTuteurs() throws NullException;

	public TuteurDto tuteurById(int parseInt) throws NullException;
	
	String putTuteur(TuteurDto tuteur) throws NullException;

	public TuteurDto addTuteur(TuteurDto tuteur) throws AjoutVideException;

	String AddElevesListeTuteur(int idE, int idT) throws NullException;

	public Integer nbrTuteur();

}
