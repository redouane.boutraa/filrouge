package com.afpa.service;

import java.util.List;

import com.afpa.dto.EmployeDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

public interface IEmployeService {

	List<EmployeDto> listeEmployes() throws NullException;
	
	EmployeDto employeById(int i)  throws NullException;
	
	EmployeDto addEmploye(EmployeDto employe) throws  AjoutVideException;
	
	String putEmploye(EmployeDto employe)  throws NullException;
	
	String deleteEmploye(int parseInt)  throws NullException;
	
	public Integer nbrEmploye();
}
