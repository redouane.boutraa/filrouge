package com.afpa.service;

import java.util.List;

import com.afpa.dto.CoursDto;
import com.afpa.dto.PersonneDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

public interface ICoursService {

	
	public String ajoutCours(CoursDto monCours) throws AjoutVideException;
	
	public List<CoursDto> listeDesCours() throws NullException;
	
	public List<PersonneDto>listeElevePourUnCours(CoursDto monCours) throws NullException;
	
	public CoursDto AfficherUnCours(CoursDto unCours) throws NullException;
	
	public String ModifierUnCours(CoursDto unCours) throws NullException;
	
	public String SupprimerUnCours(CoursDto unCours) throws NullException;

	String addEleveDansListe(int idC, int idE) throws NullException;

	String deleteEleveDansListe(int idC, int idE) throws NullException;

	int nbrCours();
	
}
