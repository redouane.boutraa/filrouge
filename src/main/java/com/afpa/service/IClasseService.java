package com.afpa.service;

import java.util.List;

import com.afpa.dto.ClasseDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.CoursException;
import com.afpa.exceptions.ElevesException;
import com.afpa.exceptions.NullException;

public interface IClasseService {
	
	public String ajoutClasse(ClasseDto maClasse) throws AjoutVideException ;
	
	public List<ClasseDto> listeDesClasses() throws NullException ;
	
	public ClasseDto AfficherUneClasse(ClasseDto maclass) throws NullException ;
	
	public String ModifierUneClasse(ClasseDto maclass)throws NullException ;
	
	public String SupprimerUneClass(ClasseDto maclass)throws NullException ;

	int nbrClasses();

}
