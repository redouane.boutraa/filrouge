package com.afpa.service;

import java.util.List;

import com.afpa.exceptions.NullException;
import com.afpa.security.model.RoleDto;

public interface IRoleService {

		List<RoleDto> findAll() throws NullException;

}
