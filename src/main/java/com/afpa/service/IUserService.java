package com.afpa.service;

import java.util.List;

import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.security.model.User;
import com.afpa.security.model.UserDto;

public interface IUserService {

	List<UserDto> findAll() throws NullException;

	UserDto add(UserDto user) throws  AjoutVideException;

	User save(User user) throws AjoutVideException;

	void delete(int id) throws NullException;

}
