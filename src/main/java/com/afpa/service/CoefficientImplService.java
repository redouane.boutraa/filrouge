package com.afpa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IDaoCoefficient;
import com.afpa.dto.CoefficientDto;
import com.afpa.entity.personne.Coefficient;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
@Service
public class CoefficientImplService implements ICoefficientService {
	
	@Autowired
	IDaoCoefficient cdao;
	
	@Autowired
	ModelMapper model;

	@Override
	public List<CoefficientDto> listeCoefficients() throws NullException{
		List<Coefficient> listCoef = (List<Coefficient>) this.cdao.findAll();
		if(listCoef == null) {
			throw new NullException();
		}else {
			List<CoefficientDto> mesCoefficients = (listCoef)
					.stream()
					.map(e -> {
						CoefficientDto c = new CoefficientDto();
						c.setId(e.getId());
						c.setLabel(e.getLabel());
						c.setCoefficient(e.getCoefficient());
						return c;
					})
					.collect(Collectors.toList());
			return mesCoefficients;
		}
	}

	@Override
	public CoefficientDto coefficientById(int i) throws NullException{
		Coefficient c = cdao.findById(i).get();
		if(c == null) {
			throw new NullException();
		}else {
			
			CoefficientDto cd = new CoefficientDto();
			cd.setId(c.getId());
			cd.setLabel(c.getLabel());
			cd.setCoefficient(c.getCoefficient());
			return cd;
		}
	}

	@Override
	public CoefficientDto addCoefficient(CoefficientDto coefficient) throws AjoutVideException{
		if(coefficient == null || coefficient.getCoefficient() == null || coefficient.getLabel() == null) {
			throw new AjoutVideException();
		}else {
			return model.map(cdao.save(model.map(coefficient, Coefficient.class)), CoefficientDto.class);
		}
	}

	@Override
	public String putCoefficient(CoefficientDto coefficient) throws NullException{
		Coefficient c = this.cdao.findById(coefficient.getId()).get();
		if(c == null) {
			throw new NullException();
		}else {
		
		c.setLabel(coefficient.getLabel());
		c.setCoefficient(coefficient.getCoefficient());
		cdao.save(c);
		return "ok. Bien modifié";
		}
	}

	@Override
	public String deleteCoefficient(int parseInt){
		cdao.deleteById(parseInt);
		return "ok bien supprimer";
	}

}
