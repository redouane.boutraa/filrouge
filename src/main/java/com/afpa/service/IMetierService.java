package com.afpa.service;

import java.util.List;

import com.afpa.dto.MetierDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

public interface IMetierService {

	public String ajoutMetier(MetierDto monMetier)throws  AjoutVideException;
	
	public List<MetierDto> listeDesMetiers() throws NullException;
	
	public MetierDto AfficherUnMetier(int  id)  throws NullException;
	
	public String ModifierUnMetier(MetierDto unMetier)  throws NullException;
	
	public String SupprimerUnMetier(int id)  throws NullException;
	
	public Integer nbrMetier();
	
}
