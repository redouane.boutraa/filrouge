package com.afpa.service;

import java.util.List;

import com.afpa.dto.CoursDto;
import com.afpa.dto.EleveDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

public interface IEleveService  {

	List<EleveDto> listeEleves() throws NullException;

	EleveDto eleveById(int i) throws NullException;

	EleveDto addEleve(EleveDto eleve) throws AjoutVideException, NullException;

	EleveDto putEleve(EleveDto eleve) throws NullException;

	String deleteEleve(int parseInt) throws NullException;

	void addcours(int idE, int idC);

	void deletecours(int idE, int idC);

	Integer nbrEleve();

	List<CoursDto> coursListe(int parseInt) throws NullException;

}
