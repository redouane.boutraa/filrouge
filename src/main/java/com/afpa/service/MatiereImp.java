package com.afpa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IDaoMatiere;
import com.afpa.dto.MatiereDto;
import com.afpa.entity.cours.Matiere;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;


@Service
public class MatiereImp implements IMatiereService{

	@Autowired
	IDaoMatiere mesMatieres;



	@Override
	public String ajoutMatiere(MatiereDto maMatiere) throws  AjoutVideException{
		if(maMatiere.getLabel() == null) {
			throw new AjoutVideException();
		}else {
			this.mesMatieres.save(Matiere.builder()
					.label(maMatiere.getLabel())
					.build());
			return "ok";
		}
	}



	@Override
	public List<MatiereDto> listeDesMatiere() throws NullException{
		List<Matiere> maListe =  (List<Matiere>) this.mesMatieres.findAll();
		if(maListe == null) {
			throw new NullException();
		}else {
			List<MatiereDto> maListeDto =  maListe.stream().map(e -> MatiereDto.builder()
					.id(e.getId())
					.label(e.getLabel())
					.build())
					.collect(Collectors.toList());
			return maListeDto;
		}
	}

	@Override
	public MatiereDto AfficherUneMatiere(MatiereDto maMat) throws NullException{
		Matiere maMatiere = this.mesMatieres.findById(maMat.getId()).get();
		if(maMatiere == null) {
			throw new NullException();
		}else {

			return MatiereDto.builder()
					.id(maMatiere.getId())
					.label(maMatiere.getLabel())
					.build();
		}
	}

	@Override
	public String ModifierUneMatiere(MatiereDto maMat) throws NullException{
		Matiere maMatiere = this.mesMatieres.findById(maMat.getId()).get();
		if(maMatiere == null) {
			throw new NullException();
		}else {

			maMatiere.setLabel(maMat.getLabel());
			this.mesMatieres.save(maMatiere);
			return "ok. Bien mis a jour";
		}
	}

	@Override
	public String SupprimerUneMatiere(MatiereDto maMat) throws NullException{
		Matiere maMatiere = this.mesMatieres.findById(maMat.getId()).get();
		if(maMatiere == null) {
			throw new NullException();
		}else {

			this.mesMatieres.delete(maMatiere);
			return "ok";
		}
	}



	@Override
	public int nbrClasses() {
		List<Matiere> maListe =  (List<Matiere>) this.mesMatieres.findAll();
		return maListe.size();
	}

}
