package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IDaoCours;
import com.afpa.dao.IDaoEleve;
import com.afpa.dto.CoursDto;
import com.afpa.dto.EleveDto;
import com.afpa.dto.PersonneDto;
import com.afpa.entity.cours.Cours;
import com.afpa.entity.personne.Eleve;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;


@Service
public class CoursImpl implements ICoursService{

	@Autowired
	IDaoCours mesCours;

	@Autowired
	IDaoEleve eleveDao;

	@Override
	public String ajoutCours(CoursDto monCours) throws AjoutVideException {
		
		if(monCours == null ||   monCours.getLabel() == null ||  monCours.getPrix() == null ||  monCours.getMatiere().getId() == 0) {
			throw new AjoutVideException();
		}else {
			this.mesCours.save(Cours.builder()
					.label(monCours.getLabel())
					.matiere(monCours.getMatiere())
					.prix(monCours.getPrix())
					.build());
			return "ok";
		}

	}


	@Override
	public List<CoursDto> listeDesCours() throws NullException {
		List<Cours> maListe =  (List<Cours>) this.mesCours.findAll();
		if(maListe == null) {
			throw new NullException();
		}else {

			List<CoursDto> maListeDto =  maListe.stream().map(e -> CoursDto.builder()
					.id(e.getId())
					.label(e.getLabel())
					.matiere(e.getMatiere())
					// .listEleves(e.getListEleves())
					.prix(e.getPrix())
					.build())
					.collect(Collectors.toList());
			return maListeDto;
		}
	}

	@Override
	public int nbrCours() {
		List<Cours> maListe =  (List<Cours>) this.mesCours.findAll();
		int nbr = maListe.size();
		return nbr;
	}

	@Override
	public CoursDto AfficherUnCours(CoursDto unCours) throws NullException{
		Cours monCour = this.mesCours.findById(unCours.getId()).get();
		if(monCour == null) {
			throw new NullException();
		}else {

			return CoursDto.builder()
					.id(monCour.getId())
					.label(monCour.getLabel())
					.matiere(monCour.getMatiere())
					.prix(monCour.getPrix())
					.build();
		}
	}


	@Override
	public String ModifierUnCours(CoursDto unCours) throws NullException{
		//System.out.println("coucou");
		Cours monCour = this.mesCours.findById(unCours.getId()).get();
		if(monCour == null) {
			throw new NullException();
		}else {

			monCour.setLabel(unCours.getLabel());
			monCour.setMatiere(unCours.getMatiere());
			monCour.setPrix(unCours.getPrix());
			this.mesCours.save(monCour);
			return "ok cours bien modifié";
		}
	}


	@Override
	public String SupprimerUnCours(CoursDto unCours) throws NullException{
		Cours monCour = this.mesCours.findById(unCours.getId()).get();
		if(monCour == null) {
			throw new NullException();
		}else {

			this.mesCours.delete(monCour);
			return "ok";
		}
	}


	@Override
	public List<PersonneDto> listeElevePourUnCours(CoursDto monCours) throws NullException{
		Cours monCour = null;
		Optional<Cours> monOp = this.mesCours.findById(monCours.getId());
		if(!monOp.isPresent()) {
			throw new NullException();
		}else {
			monCour = monOp.get();
		}
		if(monCour == null) {
			throw new NullException();
		}else {

			List<PersonneDto> mesEleve = monCour.getListEleves().stream()
					.map(e -> EleveDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.prenom(e.getPrenom())
							.dateNaissance(e.getDateNaissance())
							.build())
					.collect(Collectors.toList());
			return mesEleve;
		}
	}

	@Override
	public String addEleveDansListe(int idC, int idE) throws NullException{
		Cours co = this.mesCours.findById(idC).get();
		Eleve el = this.eleveDao.findById(idE).get();
		if(co == null || el == null) {
			throw new NullException();
		}else {

			List <Cours> listCrsElve = el.getListCours();
			boolean test = true;
			for (Cours cours : listCrsElve) {
				System.out.println(el.getNom());
				if(cours == co) {
					test = false;
					break;
				}
			}
			if(test) {
				listCrsElve.add(co);
				this.eleveDao.save(el);
			}
			return "ok";
		}


	}

	@Override
	public String deleteEleveDansListe(int idC, int idE) throws NullException{
		Cours co = this.mesCours.findById(idC).get();
		Eleve el = this.eleveDao.findById(idE).get();
		if(co == null || el == null) {
			throw new NullException();
		}else {

			List <Cours> listCrsElve = el.getListCours();
			listCrsElve.remove(co);
			this.eleveDao.save(el);
			return "ok";
		}
	}
}
