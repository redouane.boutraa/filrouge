package com.afpa.service;

import java.util.List;

import com.afpa.dto.CoefficientDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

public interface ICoefficientService {

	public List<CoefficientDto> listeCoefficients() throws NullException;
	
	public CoefficientDto coefficientById(int parseInt) throws NullException;
	
	public CoefficientDto addCoefficient(CoefficientDto coefficient) throws AjoutVideException;

	String putCoefficient(CoefficientDto coefficient) throws NullException;
	
	public String deleteCoefficient(int parseInt) throws NullException;
	
}
