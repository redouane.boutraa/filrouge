package com.afpa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IDaoMetier;
import com.afpa.dto.MetierDto;
import com.afpa.entity.metier.Metier;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;

@Service
public class MetierImpl implements IMetierService {
	
	@Autowired
	IDaoMetier mesMetiers;

	@Override
	public String ajoutMetier(MetierDto monMetier) throws  AjoutVideException{
		if(monMetier == null || monMetier.getLabel() == null) {
			throw new AjoutVideException();
		}else {
			this.mesMetiers.save(Metier.builder()
					.label(monMetier.getLabel())
					.build());
			return "Le métier " + monMetier + "  a été ajouté";
		}
	}

	@Override
	public List<MetierDto> listeDesMetiers()  throws NullException{
		List<Metier> maListe = (List<Metier>) this.mesMetiers.findAll();
		if(maListe == null) {
			throw new NullException();
		}else {
			List<MetierDto> maListeDto = maListe.stream().map(e -> MetierDto.builder()
					.id(e.getId())
					.label(e.getLabel())
					.build())
					.collect(Collectors.toList());
			return maListeDto;
		}
		
	}

	@Override
	public MetierDto AfficherUnMetier(int id)  throws NullException{
		Metier monMetier = this.mesMetiers.findById(id).get();
		if(monMetier == null) {
			throw new NullException();
		}else {
		return MetierDto.builder()
				.id(monMetier.getId())
				.label(monMetier.getLabel())
				.build();
		}
	}

	@Override
	public String ModifierUnMetier(MetierDto unMetier)  throws NullException{
		Metier monMetier = this.mesMetiers.findById(unMetier.getId()).get();
		if(monMetier == null) {
			throw new NullException();
		}else {
		monMetier.setLabel(unMetier.getLabel());
		this.mesMetiers.save(monMetier);
		return "Le métier " + unMetier + "  est mis à jour";
		}
	}

	@Override
	public String SupprimerUnMetier(int id)  throws NullException{
		this.mesMetiers.delete(this.mesMetiers.findById(id).get());
		return "Le métier est supprimé";
	}

	@Override
	public Integer nbrMetier() {
		return (int)this.mesMetiers.count();
	}

}
