package com.afpa.service;

import java.util.List;
import com.afpa.dto.MatiereDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;


public interface IMatiereService {
	
	
	public String ajoutMatiere(MatiereDto maMatiere)throws  AjoutVideException;
	
	public List<MatiereDto> listeDesMatiere() throws NullException;
	
	public MatiereDto AfficherUneMatiere(MatiereDto maMat) throws NullException;
	
	public String ModifierUneMatiere(MatiereDto maMat) throws NullException;
	
	public String SupprimerUneMatiere(MatiereDto maMat) throws NullException;

	public int nbrClasses();

}
