package com.afpa.exceptions;

public class AjoutVideException  extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AjoutVideException() {
		super("l'objet a ajouter est vide");
	}

}
