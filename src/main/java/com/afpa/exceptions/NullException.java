package com.afpa.exceptions;

public class NullException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public NullException() {
		super("la variable est null");
	}
	
	
}
