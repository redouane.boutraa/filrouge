package com.afpa.dto;


import java.util.List;

import com.afpa.security.model.UserDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class EleveDto extends PersonneDto{

	private TuteurDto Tuteur;

	private List<CoursDto> cours;
	
	private UserDto user;
	
}
