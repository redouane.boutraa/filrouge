package com.afpa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dto.CoursDto;
import com.afpa.dto.EleveDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.ElevesException;
import com.afpa.exceptions.NullException;
import com.afpa.service.IEleveService;
import com.afpa.service.ITuteurService;

@RestController
public class EleveControllers {

	@Autowired
	IEleveService eleveServ;
	

	
	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping(path = "/eleves")
	public ResponseEntity<List<EleveDto>> JsonListeEleves() throws NullException{
		return new ResponseEntity<>(this.eleveServ.listeEleves(), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('USER')")
	@GetMapping(path = "/eleves2")
	public ResponseEntity<List<EleveDto>> JsonListeEleves2() throws NullException{
		return new ResponseEntity<>(this.eleveServ.listeEleves(), HttpStatus.OK);
	}
	
	@GetMapping(path = "/eleves/{id}")
	public ResponseEntity<EleveDto> JsonElevesById(@PathVariable String id) throws NumberFormatException, NullException{
		return new ResponseEntity<>(this.eleveServ.eleveById(Integer.parseInt(id)),HttpStatus.OK);
	}
	
	@GetMapping(path = "/eleves/cours/{id}")
	public  ResponseEntity<List<CoursDto>> coursEleve(@PathVariable String id) throws NumberFormatException, NullException{
		return new ResponseEntity<>(this.eleveServ.coursListe(Integer.parseInt(id)), HttpStatus.OK);
	}
	
	
	@PostMapping(path = "/eleves")
	public EleveDto addEleve(@RequestBody EleveDto eleve) throws NullException, AjoutVideException {
		EleveDto el = this.eleveServ.addEleve(eleve);
		return el;
	}
	
	@PutMapping(path = "/eleves")
	public ResponseEntity<EleveDto> putEleve(@RequestBody EleveDto eleve) throws NullException{
		return new ResponseEntity<>(this.eleveServ.putEleve(eleve),HttpStatus.OK);
	}
	
	@GetMapping(path = "/eleves/total")
	public Integer totalEleves() throws ElevesException {
		return this.eleveServ.nbrEleve();
	}
	
	@PutMapping(path = "/listcours/{idE}/{idC}")
	public ResponseEntity<String> putListCours(@PathVariable String idE, @PathVariable String idC) {
			this.eleveServ.addcours(Integer.parseInt(idE), Integer.parseInt(idC));
			return new ResponseEntity<>("ok bien ajouté",HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/listcours/{idE}/{idC}")
	public ResponseEntity<String> delete(@PathVariable String idE, @PathVariable String idC) {
		this.eleveServ.deletecours(Integer.parseInt(idE), Integer.parseInt(idC));
		return new ResponseEntity<>("ok bien supprimé",HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/eleves/{id}")
	public ResponseEntity<String> deleteEleve(@PathVariable String id) throws NumberFormatException, NullException{
		return new ResponseEntity<>(this.eleveServ.deleteEleve(Integer.parseInt(id)),HttpStatus.OK);
	}
	
}
