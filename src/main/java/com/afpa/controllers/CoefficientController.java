package com.afpa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dto.CoefficientDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.service.ICoefficientService;

@RestController
public class CoefficientController {
	
	@Autowired
	ICoefficientService coeffserv;
	
	@GetMapping(path = "/coefficients")
	public ResponseEntity<List<CoefficientDto>> listeCoeffiecient() throws NullException{
		return new ResponseEntity<>(this.coeffserv.listeCoefficients(),HttpStatus.OK);
	}
	
	@GetMapping(path = "/coefficients/{id}")
	public ResponseEntity<CoefficientDto> coefficientById(@PathVariable String id) throws NumberFormatException, NullException{
		return new ResponseEntity<>(this.coeffserv.coefficientById(Integer.parseInt(id)),HttpStatus.OK);
	}
	
	@PostMapping(path = "/coefficients")
	public CoefficientDto addCoeffiecient(@RequestBody CoefficientDto coefficient) throws AjoutVideException {
		CoefficientDto co = this.coeffserv.addCoefficient(coefficient);
		return co;
	}
	
	@PutMapping(path = "/coefficients")
	public ResponseEntity<String> putCoefficient(@RequestBody CoefficientDto coefficient) throws NullException {
		this.coeffserv.putCoefficient(coefficient);
		return new ResponseEntity<>(this.coeffserv.putCoefficient(coefficient),HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/coefficients/{id}")
	public ResponseEntity<String> deleteCoefficient(@PathVariable String id) throws NumberFormatException, NullException {
		return new ResponseEntity<>(this.coeffserv.deleteCoefficient(Integer.parseInt(id)),HttpStatus.OK);
	}

}
