package com.afpa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dto.ClasseDto;
import com.afpa.dto.MatiereDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.service.IMatiereService;

@RestController
public class MatieresControllers {

	
	@Autowired
	IMatiereService matiereServ;


	@GetMapping(path = { "/listeMatieres"})
	public ResponseEntity<List<MatiereDto>> JsonListeCours() throws NullException{
		return new ResponseEntity<>(this.matiereServ.listeDesMatiere(),HttpStatus.OK);
	}
	
	
	@GetMapping(path = { "/nbrMatieres"})
	public int nbrsCours() {
		return this.matiereServ.nbrClasses();
		
	}
	
	@PostMapping(path = { "/ajoutMatieres"})
	public void ajoutMat(@RequestBody MatiereDto maMatiere) throws AjoutVideException {
		this.matiereServ.ajoutMatiere(maMatiere);
	}
	
	
	@PutMapping(path = { "/updateMatiere"})
	public ResponseEntity<String> updateUneClasse(@RequestBody MatiereDto maMatiere) throws NullException{
		return new ResponseEntity<>(this.matiereServ.ModifierUneMatiere(maMatiere),HttpStatus.OK);
	}
	
	@DeleteMapping(path = { "/supprimerUneMatiere/{id}"})
	public ResponseEntity<String> deleteUneClasse(@PathVariable String id) throws NullException{
		MatiereDto maMatiere = new MatiereDto();
		maMatiere.setId(Integer.parseInt(id));
		return new ResponseEntity<>(this.matiereServ.SupprimerUneMatiere(maMatiere), HttpStatus.OK);
	}
	
	
	
}
