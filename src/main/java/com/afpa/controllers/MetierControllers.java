
package com.afpa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dto.MetierDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.service.IMetierService;

@RestController
public class MetierControllers {

	@Autowired
	IMetierService metierServ;
	
	@GetMapping(path = { "/metiers" })
	public ResponseEntity<List<MetierDto>> listeMetiers() throws NullException{
		return new ResponseEntity<>(this.metierServ.listeDesMetiers(), HttpStatus.OK);
	}
	
	@GetMapping(path = { "/metiers/{id}" })
	public ResponseEntity<MetierDto> afficherMetier(@PathVariable int id) throws NullException, NumberFormatException{
		return new ResponseEntity<>(this.metierServ.AfficherUnMetier(id),HttpStatus.OK);
	}
	
	@GetMapping(path = "/metiers/total")
	public Integer nbrMetier() {
		return this.metierServ.nbrMetier();
	}
	
	@GetMapping(path = { "/metiers/{id}/label" })
	public ResponseEntity<String> afficherLabelMetier(@PathVariable int id) throws NullException, NumberFormatException{
		return new ResponseEntity<>(this.metierServ.AfficherUnMetier(id).getLabel(),HttpStatus.OK);
	}
	
	@PostMapping(path = { "/metiers" })
	public void ajoutMetier(@RequestBody MetierDto metier) throws AjoutVideException {
		this.metierServ.ajoutMetier(metier);
	}
	
	@PutMapping(path = { "/metiers" })
	public ResponseEntity<String> updateUnMetier(@RequestBody MetierDto metier) throws NullException{
		return new ResponseEntity<>(this.metierServ.ModifierUnMetier(metier), HttpStatus.OK);
	}
	
	@DeleteMapping(path = { "/metiers/{id}" })
	public ResponseEntity<String> deleteUnMetier(@PathVariable int id) throws NullException, NumberFormatException{
		return new ResponseEntity<>(this.metierServ.SupprimerUnMetier(id), HttpStatus.OK);
	}
}
//=======
//package com.afpa.controllers;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.afpa.dto.MetierDto;
//import com.afpa.service.IMetierService;
//
//@RestController
//public class MetierControllers {
//
//	@Autowired
//	IMetierService metierServ;
//	
//	@GetMapping(path = { "/listeMetier" })
//	public List<MetierDto> JsonListeCours() {
//		return this.metierServ.listeDesMetiers();
//	}
//	
//	@PostMapping(path = { "/ajoutMetier" })
//	public void ajoutMetier(@RequestBody MetierDto metier) {
//		this.metierServ.ajoutMetier(metier);
//		
//	}
//	
//	@PutMapping(path = { "/updateMetier" })
//	public void updateUnMetier(@RequestBody MetierDto metier) {
//		this.metierServ.SupprimerUnMetier(metier);
//	}
//	
//	@DeleteMapping(path = { "/supprimerUnMetier" })
//	public void deleteUnMetier(@RequestBody MetierDto metier) {
//		this.metierServ.SupprimerUnMetier(metier);
//	}
//}

