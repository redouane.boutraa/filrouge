package com.afpa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



import com.afpa.dto.TuteurDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.ElevesException;
import com.afpa.exceptions.NullException;
import com.afpa.service.ITuteurService;

@RestController
public class TuteurControllers {

	@Autowired
	ITuteurService tuteurServ;
	
	@GetMapping(path = "/tuteurs")
	public ResponseEntity<List<TuteurDto>> JsonListeTuteur() throws NullException{
		return new ResponseEntity<> (this.tuteurServ.listeTuteurs(),HttpStatus.OK);	
	}
	
	@GetMapping(path = "/tuteurs/{id}")
	public ResponseEntity<TuteurDto> JsonTuteurById(@PathVariable String id) throws NumberFormatException, NullException{
		return new ResponseEntity<> (this.tuteurServ.tuteurById(Integer.parseInt(id)),HttpStatus.OK);	
	}
	
	@GetMapping(path = "/tuteurs/total")
	public Integer nbrTuteur() throws ElevesException {
		return this.tuteurServ.nbrTuteur();	
	}
	
	@PostMapping(path = "/tuteurs")
	public TuteurDto addTuteur(@RequestBody TuteurDto tuteur) throws ElevesException, AjoutVideException {
		TuteurDto tu  = this.tuteurServ.addTuteur(tuteur);
		return tu;
	}
	
	
	@PutMapping(path = "/tuteursListEleve/{id}/{id2}")
	public ResponseEntity<String> addTuteurEleve(@PathVariable String id,@PathVariable String id2) throws NumberFormatException, NullException {
		return new ResponseEntity<>(this.tuteurServ.AddElevesListeTuteur(Integer.parseInt(id), Integer.parseInt(id2)),HttpStatus.OK);
	}
	
	@PutMapping(path = "/tuteurs")
	public ResponseEntity<String> putTuteur(@RequestBody TuteurDto tuteur) throws NullException{
		return new ResponseEntity<>(this.tuteurServ.putTuteur(tuteur),HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/tuteurs/{id}")
	public ResponseEntity<String> deleteTuteur(@PathVariable String id) throws NumberFormatException, NullException{
		return new ResponseEntity<>( this.tuteurServ.deleteTuteur(Integer.parseInt(id)),HttpStatus.OK);
	}
	
}
