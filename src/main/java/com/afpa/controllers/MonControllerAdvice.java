package com.afpa.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



import com.afpa.dto.ReponseDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;



@ControllerAdvice
public class MonControllerAdvice {
	

	@ExceptionHandler(AjoutVideException.class)
	public ResponseEntity<Object> repObjetVide (AjoutVideException ex){
		ReponseDto rep = new ReponseDto(HttpStatus.NOT_ACCEPTABLE.value(),ex.getMessage());
		return new ResponseEntity <>(rep, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler(NullException.class)
	public ResponseEntity<Object> repObjetVide (NullException ex){
		ReponseDto rep = new ReponseDto(HttpStatus.NOT_FOUND.value(),ex.getMessage());
		return new ResponseEntity <>(rep, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<Object> repObjetVide (){
		ReponseDto rep = new ReponseDto(HttpStatus.BAD_REQUEST.value(),"l'un des id passé est incorrecte");
		return new ResponseEntity <>(rep, HttpStatus.BAD_REQUEST);
	}

}
