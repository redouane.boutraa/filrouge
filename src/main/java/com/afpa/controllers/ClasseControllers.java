package com.afpa.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.swing.text.MaskFormatter;
import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dto.ClasseDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.CoursException;
import com.afpa.exceptions.NullException;
import com.afpa.service.IClasseService;

@RestController
public class ClasseControllers {

	@Autowired
	IClasseService classServ;

	
	
	@GetMapping(path = { "/nbrClasses"})
	public int nbrsCours() {
		return this.classServ.nbrClasses();
		
	}

	@GetMapping(path = { "/listeClasses"})
	public ResponseEntity<List<ClasseDto>> JsonListeClasses() throws NullException{
		return new ResponseEntity<>(this.classServ.listeDesClasses(), HttpStatus.OK);

	}
	
	@PostMapping(path = { "/ajoutClasses"})
	public void ajoutClasse(@RequestBody ClasseDto maclass) throws AjoutVideException {
		this.classServ.ajoutClasse(maclass);
	}
	
	
	
	@PutMapping(path = { "/updateClasses"})
	public ResponseEntity<String> updateUneClasse(@RequestBody ClasseDto maclass) throws NullException{
		return new ResponseEntity<>(this.classServ.ModifierUneClasse(maclass), HttpStatus.OK);
	}
	
	@DeleteMapping(path = { "/supprimerUneClasses/{id}"})
	public ResponseEntity<String> deleteUneClasse(@PathVariable String id) throws NullException{
		ClasseDto maclass = new ClasseDto();
		maclass.setId(Integer.parseInt(id));
		return new ResponseEntity<> (this.classServ.SupprimerUneClass(maclass), HttpStatus.OK);
	}
	
	


}
