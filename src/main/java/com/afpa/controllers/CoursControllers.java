package com.afpa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dto.CoefficientDto;
import com.afpa.dto.CoursDto;
import com.afpa.dto.PersonneDto;
import com.afpa.exceptions.AjoutVideException;
import com.afpa.exceptions.NullException;
import com.afpa.service.ICoursService;

@RestController
public class CoursControllers {

	
	@Autowired
	ICoursService coursServ;
	
	

	@GetMapping(path = { "/listeCours"})
	public ResponseEntity<List<CoursDto>> JsonListeCours() throws NullException{
		return new ResponseEntity<>( this.coursServ.listeDesCours(),HttpStatus.OK);
	}
	
	@GetMapping(path = { "/nbrCours"})
	public int nbrsCours() {
		return this.coursServ.nbrCours();
		
	}
	
	@PostMapping(path = { "/ajoutCours"})
	public void ajoutCours(@RequestBody CoursDto courss) throws AjoutVideException {
		this.coursServ.ajoutCours(courss);
	}
	
	
	
	@PutMapping(path = { "/updateCours"})
	public ResponseEntity<String> updateUnCours(@RequestBody CoursDto courss) throws NullException{
		return new ResponseEntity<>(this.coursServ.ModifierUnCours(courss), HttpStatus.OK);
	}
	
	@DeleteMapping(path = { "/supprimerUnCours/{id}"})
	public ResponseEntity<String> deleteUnCours(@PathVariable String id) throws NullException, NumberFormatException{
		CoursDto courss = new CoursDto();
		courss.setId(Integer.parseInt(id));
		return new ResponseEntity<>(this.coursServ.SupprimerUnCours(courss),HttpStatus.OK);
	}
	
	@GetMapping(path = { "/listeElevePourUnCours/{id}"})
	public ResponseEntity<List<PersonneDto>> listeElevePourUnCours(@PathVariable String id) throws NullException, NumberFormatException{
		CoursDto courss = new CoursDto();
		courss.setId(Integer.parseInt(id));
		return new ResponseEntity<>(this.coursServ.listeElevePourUnCours(courss), HttpStatus.OK);

	}
	
	@GetMapping(path = {"/afficheUncours/{id}"})
	public ResponseEntity<CoursDto> afficherUncours(@PathVariable String id) throws NullException, NumberFormatException{
		CoursDto courss = new CoursDto();
		courss.setId(Integer.parseInt(id));
		return new ResponseEntity<>(this.coursServ.AfficherUnCours(courss), HttpStatus.OK);
	}
	
	
	@PostMapping(path = {"/addElvsDansCours/{idE}/{idC}"})
	public ResponseEntity<String> addElvCours(@PathVariable String idE, @PathVariable String idC) throws NumberFormatException, NullException {
		return new ResponseEntity<>(this.coursServ.addEleveDansListe(Integer.parseInt(idC), Integer.parseInt(idE)), HttpStatus.OK);
	}
	
	@DeleteMapping(path = {"/delElvsDansCours/{idE}/{idC}"})
	public ResponseEntity<String> delElvCours(@PathVariable String idE, @PathVariable String idC) throws NumberFormatException, NullException{
		return new ResponseEntity<>(this.coursServ.deleteEleveDansListe(Integer.parseInt(idC), Integer.parseInt(idE)), HttpStatus.OK);
	}
	
	
}
