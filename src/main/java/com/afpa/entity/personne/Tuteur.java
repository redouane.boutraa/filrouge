package com.afpa.entity.personne;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.afpa.security.model.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Tuteur extends Adulte {
	

	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
	private List<Eleve> eleves;
	
	@ManyToOne
	private Coefficient coefficient;
	
	@Column(unique=true, nullable = false)
	private String email;
	
	@Column(name = "emailValidation", unique=true, nullable = false)
	private boolean emailValidation;
	
	@OneToOne
	User user;
	
}
