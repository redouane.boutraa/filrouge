package com.afpa.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.afpa.security.model.Role;

public interface IDaoRole extends CrudRepository<Role, Integer> {

	public Optional<Role> findByLabel(String string);

}
